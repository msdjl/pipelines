package com.mycompany.app;

import org.testng.annotations.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;


public class CalculatorTest {
    @Test
    public void multiplyPositiveNumbers() throws Exception {
        assertThat(20, equalTo(Calculator.multiply(4, 5)));
    }

    @Test
    public void addPositiveNumber() throws Exception {
        assertThat(3, equalTo(Calculator.add(1, 2)));
    }

    @Test
    public void addNegativeNumber() throws Exception {
        assertThat(-1, equalTo(Calculator.add(1, -2)));
    }

    @Test
    public void subNegativeNumber() throws Exception {
        assertThat(3, equalTo(Calculator.sub(1, -2)));
    }

    @Test
    public void pseudoTest() throws Exception {
        Calculator calculator = new Calculator();
        calculator.m();
    }
}